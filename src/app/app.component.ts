import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  posts: Post[] = [
    {
      id: 1, 
      title:"React", 
      text: "JavaScript-библиотека для создания пользовательских интерфейсов"
    },
    {
      id: 2, 
      title:"Angular", 
      text: "Angular is an app-design framework and development platform for creating efficient and sophisticated single-page apps."
    },
    {
      id: 3, 
      title:"Vue", 
      text: "Vue (pronounced /vjuː/, like view) is a progressive framework for building user interfaces"
    },
    {
      id: 4, 
      title:"Node.js", 
      text: "Node.js® — це JavaScript–оточення побудоване на JavaScript–рушієві Chrome V8."
    }
  ];

  ngOnInit() {
    this.posts.reverse();
  }

  generateNewId(): number {
    return this.posts[0].id + 1;
  }

  updatePosts(event): void {
    this.posts.unshift({ ...event,
      id: this.generateNewId(),
    });
  }

  removeIdPost(id:number) {
    this.posts = this.posts.filter((item) => item.id !== id);
  }
}

export  interface  Post {
  title: string;
  text: string;
  id?: number;
}